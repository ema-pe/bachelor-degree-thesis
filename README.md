# BSc degree thesis: design and development of APIs in Go for a domain-specific messaging system on SeismoCloud

[Direct PDF download](https://gitlab.com/ema-pe/bachelor-degree-thesis/-/releases/v1) of both thesis and slides.

Please note that thesis and slides are written in Italian.

Licensed under the Creative Commons Attribution Share Alike 4.0 International.
